https://gitlab.com/yoosofan_course/ie982_code.git

git config --global user.name "Ahmad Yoosofan"
git config --global user.email "yoosofan@fastmail.fm"

Create a new repository
git clone git@gitlab.com:yoosofan_course/ie982_code.git
cd ie982_code
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:yoosofan_course/ie982_code.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:yoosofan_course/ie982_code.git
git push -u origin --all
git push -u origin --tags
